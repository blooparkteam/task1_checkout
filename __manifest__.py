# -*- coding: utf-8 -*-

{
    "name": "Checkout.com payment Acquirer",
    "version": "0.1",
    "depends": ['website','website_sale','payment',"l10n_de"],
    "author": "Nadia AFAKROUCH(nadia.afa@gmail.com)",
    "license": 'AGPL-3',
    "description": """
    
    Checkout.com payment Acquirer
    """,
    "summary": "",
    "website": "",
    "category": 'Website',
    "data": ['views/payment_checkout_templates.xml',
             'data/payment_acquirer_data.xml',
        'views/payment_views.xml',

             
    ],
    "qweb": ['static/src/xml/*.xml'],
    "auto_install": True,
    "installable": True,
    "application": False,
}
