odoo.define('task1_checkout.checkout', function (require) {
    "use strict";

    var ajax = require('web.ajax');
    var core = require('web.core');
    var _t = core._t;
    var qweb = core.qweb;

    window.CKOConfig = {
        publicKey: 'pk_test_c44626d2-48b4-43ed-ae96-0328e2f514d4',
        value: parseInt($("input[name='amount']").val()) * 100,
        currency: $("input[name='currency']").val(),
        customerEmail: $("input[name='email']").val(),
        appMode: 'lightbox',
        renderMode: 0,
        cardTokenised: function (event) {

            ajax.jsonRpc("/payment/checkout/transaction", 'call', {
                cardToken: event.data.cardToken,
                email: $("input[name='email']").val(),
                customer_name: $("input[name='name']").val(),
                currency: $("input[name='currency']").val(),
                address_line1: $("input[name='address_line1']").val(),
                address_zip: $("input[name='address_zip']").val(),
                address_country_code: $("input[name='address_country_code']").val(),
                partner_country_id: $("input[name='partner_country_id']").val(),
                address_city: $("input[name='address_city']").val(),
                phone: $("input[name='phone']").val(),
                amount: $("input[name='amount']").val() * 100,
                invoice_num: $("input[name='invoice_num']").val(),

            }).done(function (data) {
                console.log("Ajax Done");
                console.log(data);
                console.log($("input[name='partner_country_id']").val());
                console.log(data["status"]);
                if (data["status"] == "Authorised") {
                    $(".payment-form").html('<div class="alert alert-success">Paiement Success</div>');

                    ajax.jsonRpc('/payment/checkout/s2s/create_json', 'call', {
                        partner_id: $("input[name='partner_id']").val(),
                        acquirer_id: $("#acquirer_checkout").val(),
                        acquirer_ref: data["trackId"],
                    }).then(function (token_id) {

                        ajax.jsonRpc('/checkout/save', 'call', {
                            amount: $("input[name='amount']").val() * 100,
                            currency_id: $("input[name='currency_id']").val(),
                            acquirer_id: $("#acquirer_checkout").val(),
                            partner_id: parseInt($("input[name='partner_id']").val()),
                            country_id: parseInt($("input[name='partner_country_id']").val()),
                            payment_token_id: token_id,

                        }).then(function (order_id) {

                            window.location.href = "confirmation";
                        });
                    });

                }
                else {
                    $(".payment-form").html('<div class="alert alert-danger">Oooops, the paiement was not successful, Please Verify Your Infos</div>');
                }

            }).fail(function () {
                console.log("Ajax Failed");
                $(".payment-form").html('<div class="alert alert-danger">An Error Accured, Please try again</div>');
            });


        }
    };


});
