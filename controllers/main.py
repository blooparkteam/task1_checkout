# -*- coding: utf-8 -*-


from odoo import http
from pprint import pprint as pp
import requests
import json
from odoo.http import request
from odoo import http, tools, _


class CheckoutController(http.Controller):
    @http.route(['/payment/checkout/transaction'], type='json', auth='public')
    def checkout_transaction(self, **post):
        """ Create a payment transaction """

        payload = '{"autoCapTime":"24","autoCapture":"Y","chargeMode":1,"email":"' + post[
            "email"] + '","customerName":"' + post[
                      "customer_name"] + '","description":"charge description","value":"' + str(
            post["amount"]) + '","currency":"' + post[
                      "currency"] + '","trackId":"TRK12345","transactionIndicator":"1","customerIp":"96.125.185.51","cardToken":"' + \
                  post["cardToken"] + '","shippingDetails":{"addressLine1":"' + post[
                      "address_line1"] + '","postcode":"' + post["address_zip"] + '","country":"' + post[
                      "address_country_code"] + '","city":"' + post["address_city"] + '","phone":{"number":"' + post[
                      "phone"] + '"}},}'
        headers = {'Authorization': 'sk_test_d9578598-93db-40e8-9bc5-02ebb2652c9a',
                   'Content-Type': 'application/json;charset=UTF-8'}
        response = requests.post("https://sandbox.checkout.com/api2/v2/charges/token", data=payload, headers=headers)
        print response.json()
        return response.json()

    @http.route(['/shop/checkout/confirmation'], type='http', auth="public", website=True)
    def checkout_confirmation(self, **post):
        """ Create a payment confirmation """
        order = request.website.sale_get_order()
        order.with_context(send_email=True).action_confirm()
        tx = request.env['payment.transaction'].sudo().search([('sale_order_id', '=', order.id)], limit=1)

        return ''

    @http.route(['/payment/checkout/s2s/create_json'], type='json', auth='public')
    def checkout_s2s_create_json(self, **kwargs):
        """Create token"""
        acquirer_id = int(kwargs.get('acquirer_id'))
        acquirer = request.env['payment.acquirer'].browse(acquirer_id)
        return acquirer.s2s_process(kwargs)

    @http.route(['/checkout/save'], type='json', auth="public", website=True)
    def transaction(self, amount, currency_id, acquirer_id, partner_id, country_id, payment_token_id):
        order = request.website.sale_get_order()

        order.with_context(send_email=True).action_confirm()
        order.state = 'done'

        # manage exception on sentry
        invoice_id = order.action_invoice_create()


        values = {
            'acquirer_id': int(acquirer_id),
            'reference': order.name,
            'amount': float(amount),
            'currency_id': int(currency_id),
            'partner_id': partner_id,
            'partner_country_id': country_id,
            'sale_order_id': order.id,
            'payment_token_id': payment_token_id,
            'state': 'done',
        }

        tx = request.env['payment.transaction'].sudo().create(values)
        request.session['website_payment_tx_id'] = tx.id
        return order.id

