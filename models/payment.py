# coding: utf-8
from odoo import api, fields, models, _


class AcquirerPaypal(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('checkout', 'Checkout.com')])
    checkout_public_key = fields.Char('Checkout public key', required_if_provider='checkout', groups='base.group_user')
    checkout_secret_key = fields.Char('Checkout secret key', required_if_provider='checkout', groups='base.group_user')

    @api.multi
    def checkout_form_generate_values(self, tx_values):
        self.ensure_one()
        checkout_tx_values = dict(tx_values)
        temp_checkout_tx_values = {
            'company': self.company_id.name,
            'amount': tx_values.get('amount'),
            'currency': tx_values.get('currency') and tx_values.get('currency').name or '',
            'currency_id': tx_values.get('currency') and tx_values.get('currency').id or '',
            'address_line1': tx_values['partner_address'],
            'address_city': tx_values['partner_city'],
            'partner_country_id': tx_values['partner_country_id'],
            'address_country': tx_values['partner_country'] and tx_values['partner_country'].name or '',
            'address_country_code': tx_values['partner_country'] and tx_values['partner_country'].code or '',
            'email': tx_values['partner_email'],
            'address_zip': tx_values['partner_zip'],
            'name': tx_values['partner_name'],
            'phone': tx_values['partner_phone'],
            'partner': tx_values['partner'],
            'partner_id': tx_values.get('partner') and tx_values.get('partner').id or '',

        }

        temp_checkout_tx_values['returndata'] = checkout_tx_values.pop('return_url', '')
        checkout_tx_values.update(temp_checkout_tx_values)
        return checkout_tx_values

    @api.model
    def checkout_s2s_form_process(self, data):
        payment_token = self.env['payment.token'].sudo().create({
            'acquirer_ref': data['acquirer_id'],
            'acquirer_id': int(data['acquirer_id']),
            'partner_id': int(data['partner_id'])
        })
        return payment_token.id
